﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class F
    {
        public Int32 i1;
        public Int32 i2;
        public Int32 i3;
        public Int32 i4;
        public Int32 i5;
        public static F Get => new F {i1 = 1, i2 = 2, i3 =3, i4 = 4, i5 = 5 };
        public override string ToString()
        {
            return $"Instance F: i1 = {i1}, i2 = {i2}, i3 = {i3}, i4 = {i4}, i5 = {i5}";
        }
        
    }
}
