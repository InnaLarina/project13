﻿using System;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;


namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            //JSON
            var cJSON = F.Get;
            string sJSON = "";

            //serialization
            var t = DateTime.UtcNow;
            for (int i = 0; i < 1_000_000; i++)
            {
                sJSON = Newtonsoft.Json.JsonConvert.SerializeObject(cJSON);
                
            }
            Console.WriteLine("After JSON serialization:");
            Console.WriteLine(sJSON);
            Console.WriteLine();

            var t2 = DateTime.UtcNow;
            Console.WriteLine($"JSON Serialization process lasted {t2 - t}");
            Console.WriteLine();

            //десериализация
            F fdesJSON = new F();
            for (int i = 0; i < 1_000_000; i++)
            {
                fdesJSON = Newtonsoft.Json.JsonConvert.DeserializeObject<F>(sJSON);
            }
            var t3 = DateTime.UtcNow;
            Console.WriteLine("After Json deserialization:");
            Console.WriteLine(fdesJSON.ToString());
            Console.WriteLine();
            
            Console.WriteLine($"Json Deserialization process lasted {t3 - t2}");
            Console.WriteLine();

            //Reflection
            var c = F.Get;
            string sReflextion = "";
            t = DateTime.UtcNow;
            for (int i = 0; i < 1_000_000; i++)
            {
                sReflextion = c.ToScv();
            }
            Console.WriteLine("After Reflextion serialization:");
            Console.WriteLine(sReflextion);
            Console.WriteLine();

            t2 = DateTime.UtcNow;
            Console.WriteLine($"Reflextion serialization process lasted {t2 - t}");
            Console.WriteLine();

            //десериализация
            F fdesReflextion = new F();
            for (int i = 0; i < 1_000_000; i++)
            {
               fdesReflextion = (F)fdesReflextion.ObjectFromScv(sReflextion);
            }
             t3 = DateTime.UtcNow;
            
            Console.WriteLine("After Reflextion deserialization:");
            Console.WriteLine(fdesReflextion.ToString());
            Console.WriteLine();
            Console.WriteLine($"Reflextion deserialization process lasted {t3 - t2}");


        }




    }
    public static class SerializatorTXT
    {
        public static string ToScv(this Object o) => String.Join(";", o.GetType().GetFields().Select(x => x.Name + "=" + x.GetValue(o)));
        public static object ObjectFromScv(this Object o,string sReflextion) 
        {
            F fdesReflextion = new F();
            FieldInfo[] fi = fdesReflextion.GetType().GetFields().ToArray();
            Match m;
            string[] arr_ser = sReflextion.Split(';');
            for (int j = 0; j < arr_ser.Count(); j++)
            {
                string RefPattern = @"=\s*\d*\s*";
                m = Regex.Match(arr_ser[j], RefPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled, TimeSpan.FromSeconds(1));
                if (m.Success)
                {
                    int val = Convert.ToInt32(arr_ser[j].Substring(arr_ser[j].IndexOf(m.ToString()) + 1));
                    fi[j].SetValue(fdesReflextion, val);
                }
            }
            return fdesReflextion;
        }
    }
    
}
